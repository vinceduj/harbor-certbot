# Harbor configuration for Certbot

Allow Certbot to run behind Harbor's nginx proxy.

## Installation

Move docker-compose.override.yml and nginx configuration (common/config/nginx/conf.d) to Harbor installation folder.

Then Certbot can be run with options `--webroot --webroot-path /usr/share/nginx/html/` behind the Harbor'snginx proxy.

Add a post-deploy hook script to restart nginx after certificates are renewed: `docker-compose exec proxy sh -c "nginx -s reload"`.
